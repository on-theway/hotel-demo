package cn.itcast.hotel;

import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import cn.itcast.hotel.service.IHotelService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpHost;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;

import static cn.itcast.hotel.constants.HotelConstants.MAPPING_TEMPLATE;

/**
 * @author:马立皓
 * @time:21:15 2022/8/15
 */
@SpringBootTest
public class HotelDocumentTest {

    @Autowired
    private IHotelService iHotelService;

    private RestHighLevelClient client;

    @BeforeEach
    void setUp(){
        this.client=new RestHighLevelClient(RestClient.builder(
                HttpHost.create("http://192.168.88.101:9200")
        ));
    }

    @AfterEach
    void tearDown() throws IOException{
        this.client.close();
    }

   @Test
    void testAddDocumentTest() throws IOException {

        //根据id查询酒店数据
       Hotel hotel = iHotelService.getById(61083L);
       //转换为文档类型
       HotelDoc hotelDoc = new HotelDoc(hotel);


       //1.准备Request对象
       IndexRequest request = new IndexRequest("hotel").id(hotelDoc.getId().toString());
       //2.准备json文档
        request.source(JSON.toJSONString(hotelDoc),XContentType.JSON);
       //3.发送请求
       client.index(request,RequestOptions.DEFAULT);
   }

    @Test
    void testGetDocumentTest() throws IOException {
        //1，准备request
        GetRequest request = new GetRequest("hotel","61083");
        //2.发送请求
        GetResponse documentFields = client.get(request, RequestOptions.DEFAULT);
        //3.解析相应结果
        String json = documentFields.getSourceAsString();
        HotelDoc hotelDoc = JSON.parseObject(json,HotelDoc.class);
        System.out.println(hotelDoc);
    }

    @Test
    void testUpdateDocument() throws IOException {
        //1.准备request
        UpdateRequest request = new UpdateRequest("hotel", "61083");
        //2.准备请求参数
        request.doc(
                "price" , "996",
                "starName" , "四钻"
        );
        //3.发送请求
        client.update(request,RequestOptions.DEFAULT);
    }

    @Test
    void testDeleteDocument() throws IOException {
        DeleteRequest request = new DeleteRequest("hotel","61083");

        client.delete(request,RequestOptions.DEFAULT);
    }

    //批量处理
    @Test
    void testBulkRequest() throws IOException {
        //批量查询
        List<Hotel> list = iHotelService.list();


        //创建request
        BulkRequest request = new BulkRequest();
        //准备参数，添加多个新增的request
        list.forEach(hotel -> {
            HotelDoc hotelDoc = new HotelDoc(hotel);
            request.add(new IndexRequest("hotel").id(hotelDoc.getId().toString())
                    .source(JSON.toJSONString(hotelDoc),XContentType.JSON));
        });
        //发送请求
        client.bulk(request,RequestOptions.DEFAULT);
    }


}

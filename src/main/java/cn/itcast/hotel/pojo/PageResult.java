package cn.itcast.hotel.pojo;

import lombok.Data;

import java.util.List;

/**
 * @author:马立皓
 * @time:19:29 2022/8/17
 */
@Data
public class PageResult {

    private Long total;

    private List<HotelDoc> hotels;

    public PageResult() {
    }

    public PageResult(Long total, List<HotelDoc> hotels) {
        this.total = total;
        this.hotels = hotels;
    }
}

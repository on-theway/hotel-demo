package cn.itcast.hotel.pojo;

import lombok.Data;

import java.sql.DataTruncation;

/**
 * @author:马立皓
 * @time:19:26 2022/8/17
 */
@Data
public class RequestParams {

    private String key;

    private Integer page;

    private Integer size;

    private String sortBy;

    private String city;

    private String brand;

    private String starName;

    private Integer minPrice;

    private Integer maxPrice;

    private String location;


}
